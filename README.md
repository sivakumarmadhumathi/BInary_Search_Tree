import java.util.Scanner;
class Node{
	public int data;
	public Node lchild;
	public Node rchild;

	/*constructor*/
	public Node()
	{	
		data = 0;
		lchild = null;
		rchild = null;
	}

	/*constructor*/
	public Node(int d,Node l,Node r)
	{
		data = d;
		lchild = l;
		rchild = r;
	}


	
}
class BinarySearchTree{
		
		public static Node root;
		public static Node temp;
		/*constructor*/
		public BinarySearchTree()
			{
				root =null;
			}
			/*method to insert*/
	        public static Node insertElement(Node r,int val)
       		{
               		 if(r == null)
               		 {
                       		 r = new Node(val,null,null);
                       		 return r;
              		  }
               		 else if(val<r.data)
                        	        r.lchild = insertElement(r.lchild,val);

               		 else if(val>r.data)
                        	        r.rchild = insertElement(r.rchild,val);

               		 return r;
      		  }

       		 /*method to search for an element*/
       		 public static boolean searchElement(Node root,int val)
       		 {
               		 if(root == null)
                        	return false;

               		 else
               		 {
                       		 while(root!=null)
                       		 {
                               		 if(val < root.data)
                                        	root = root.lchild;

                                	else if(val > root.data)
                                       		 root = root.rchild;

                               		 else
                                       		 return true;
                      		  }

              		  }return false;
       		 }
		/*method to inorder traversal*/
	        public static int inorderTraversal(Node root)
        	{
                	if(root == null)
                        	return 0;

               		 else	
               		 {
                     	   inorderTraversal(root.lchild);
                      	  System.out.println(root.data + " ");
                       	  inorderTraversal(root.rchild);
               		 }
               		 return 0;
       		 }

       		 /*method to preorder traversal*/
   	      public static int preorderTraversal(Node root)
       		 {
              		  if(root == null)
                       		 return 0;

              		  else
              		  {
                       		 System.out.println(root.data + " ");
                       		 inorderTraversal(root.lchild);
                       		 inorderTraversal(root.rchild);
              		  }
               		 return 0;
      		  }

       		 /*method to postorder traversal*/
       		 public static int postorderTraversal(Node root)
       		 {
               		 if(root == null)
                      		  return 0;
               		 else
               		 {
                   	     postorderTraversal(root.lchild);
                       	     postorderTraversal(root.rchild);
                             System.out.println(root.data+ " " );
               		 }
                	 return 0;
       		 }

		/*method to delete element*/
		public static Node  deleteElement(Node root,int val)
		{
			//traversal
			if(root == null)
				return null;

			else if(val<root.data)
				root.lchild = deleteElement(root.lchild,val);
		
			else if(val > root.data)
				root.rchild = deleteElement(root.rchild,val);

			else
			{
				if(root.rchild == null && root.rchild == null)
					root = null;
	
				else if(root.rchild == null)
				{
					root = root.lchild;
					root.lchild = null;
				}
	
				//node to be deleted has only right child
				else if(root.lchild == null)
				{
					root = root.rchild;
					root.rchild = null;
				}

				//node has two children
				else
				{
					temp.data = inorderTraversal(root);
					root.data = temp.data;
					temp = null;
				}
			}
	return root;
	}

	
		//method to delete an element from a tree
		public static Node delete(Node root,int val)
		{
			//if tree is empty
			if(root == null)
				return root;

			else if(val<root.data)
				root.lchild = delete(root.lchild,val);

			else if(val >root.data)
				root.rchild = delete(root.rchild,val);//recursively recuuring down.

			//reached the particular node
			else
			{
				if(root.lchild == null & root.rchild == null)
					return null;

				//with only one child
				else if(root.lchild==null)
					return root.rchild;

				else if(root.rchild == null)
					return root.lchild;

				else
				{
					root.data = findMin(root.rchild);//finding the inorder successor
					root.rchild= delete(root.rchild,val);//deleteing inorder succesor
				}
				return root;
			}return root;
		}

		//method to find the minimum element
		public static int findMin(Node root)
		{
			int findminv = root.data;
			while(root.lchild!=null)
			{
				findminv = root.lchild.data;
				root = root.lchild;
			}return findminv;
		}

		//method to find maximum element
		public static int findMax(Node root)
		{
			int findmaxv = root.data;
			while(root.rchild!=null)
			{
				findmaxv = root.rchild.data;
				root = root.rchild;
			}return findmaxv;
		}
			
		public static void main(String args[])
		{
			Scanner scan = new Scanner(System.in);
			int ch, num,s,i,p;
			do
			{
				System.out.println("Menu\n1.insert\n2.inorder traversal\n3.preorder traversal\n4.postorder traversal\n5.search\n6.delete\n7.indMin\n8.findMax");
				System.out.println("enter your option");
				s = scan.nextInt();
				switch(s)
				{
					case 1:System.out.println("enter the element to be inserted");
						i = scan.nextInt();
						root = insertElement(root,i);
						break;
						
					case 2:
						System.out.println("inorder traversal is:");
						inorderTraversal(root);
						break;
	
					case 3:
						System.out.println("preorder traversal is:");
						preorderTraversal(root);
						break;
		
					case 4:
						System.out.println("post order traversal is:");
						postorderTraversal(root);
						break;
					
					case 5:
						System.out.println("enter elemetnts to be searched:");
						p = scan.nextInt();
						if(searchElement(root,p))
							System.out.println("element found");
						else
							System.out.println("element not found");
						break;

					case 6:
						System.out.println("Enter the element you want to delete");
						delete(root,scan.nextInt());
						break;

					case 7:
						System.out.println("The minimum value is:" + findMin(root) );
						break;

					case 8:System.out.println("The maximum value is: " + findMax(root));
						break;

					}
				System.out.println("press 1 to continue");
				ch = scan.nextInt();
				}while(ch==1);
		}
}